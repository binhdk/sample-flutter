import 'package:drink_order/main/launcher.dart';
import 'package:drink_order/main/main_page.dart';
import 'package:drink_order/main/user/forgot_password/forgot_password_page.dart';
import 'package:drink_order/main/user/forgot_password/set_password_page.dart';
import 'package:drink_order/main/user/forgot_password/verify_forgot_password_phone_page.dart';
import 'package:drink_order/main/user/login_page.dart';
import 'package:drink_order/main/user/register/register_page.dart';
import 'package:drink_order/main/user/register/register_password_page.dart';
import 'package:drink_order/main/user/register/verify_register_phone_page.dart';
import 'package:drink_order/res/localization/localization.dart';
import 'package:drink_order/res/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(
    // For widgets to be able to read providers, we need to wrap the entire
    // application in a "ProviderScope" widget.
    // This is where the state of our providers will be stored.
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      theme: AppTheme.theme,
      initialRoute: Launcher.routeName,
      routes: {
        Launcher.routeName: (context) => const Launcher(),
        MainPage.routeName: (context) => const MainPage(),
        LoginPage.routeName: (context) => const LoginPage(),
        RegisterPage.routeName: (context) => const RegisterPage(),
        VerifyRegisterPhonePage.routeName: (context) =>
            const VerifyRegisterPhonePage(),
        RegisterPasswordPage.routeName: (context) =>
            const RegisterPasswordPage(),
        ForgotPasswordPage.routeName: (context) => const ForgotPasswordPage(),
        VerifyForgotPasswordPhonePage.routeName: (context) =>
            const VerifyForgotPasswordPhonePage(),
        SetPasswordPage.routeName: (context) => const SetPasswordPage(),
      },
    );
  }
}
