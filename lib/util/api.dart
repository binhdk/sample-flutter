const baseUrl = 'http://127.0.0.1:5000/api/';

//user apis
const loginUrl = '${baseUrl}user/login';
const logoutUrl = '${baseUrl}user/logout';
const profileUrl = '${baseUrl}user/profile';
const userUrl = '${baseUrl}user';
const usersUrl = '${baseUrl}users';
