import 'package:drink_order/util/animation.dart';
import 'package:flutter/material.dart';

class EnterExitSlideLeftToRightRoute extends PageRouteBuilder {
  final Widget enterPage;
  final Widget exitPage;
  final RouteSettings? routeSettings;

  EnterExitSlideLeftToRightRoute({
    this.routeSettings,
    required this.enterPage,
    required this.exitPage,
  }) : super(
          settings: routeSettings,
          pageBuilder: (context, animation, secondaryAnimation) => enterPage,
          transitionsBuilder: (context, animation, secondaryAnimation, child) =>
              Stack(
            children: [
              slideOutOfLeft(exitPage, animation),
              slideInFromRight(enterPage, animation),
            ],
          ),
        );
}
