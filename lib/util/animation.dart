import 'package:flutter/material.dart';

AnimatedWidget slideInFromBottom(Widget widget, Animation animation) {
  const begin = Offset(0.0, 1.0);
  const end = Offset.zero;
  const curve = Curves.ease;
  final tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
  return SlideTransition(
    position: animation.drive(tween),
    child: widget,
  );
}

AnimatedWidget slideOutOfTop(Widget widget, Animation animation) {
  const begin = Offset.zero;
  const end = Offset(0.0, -1.0);
  const curve = Curves.ease;
  final tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
  return SlideTransition(
    position: animation.drive(tween),
    child: widget,
  );
}

AnimatedWidget slideInFromLeft(Widget widget, Animation animation) {
  const begin = Offset.zero;
  const end = Offset(1.0, 0.0);
  const curve = Curves.ease;
  final tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
  return SlideTransition(
    position: animation.drive(tween),
    child: widget,
  );
}

AnimatedWidget slideOutOfLeft(Widget widget, Animation animation) {
  const begin = Offset.zero;
  const end = Offset(-1.0, 0.0);
  const curve = Curves.ease;
  final tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
  return SlideTransition(
    position: animation.drive(tween),
    child: widget,
  );
}

AnimatedWidget slideInFromRight(Widget widget, Animation animation) {
  const begin = Offset(1.0, 0.0);
  const end = Offset.zero;
  const curve = Curves.ease;
  final tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
  return SlideTransition(
    position: animation.drive(tween),
    child: widget,
  );
}

AnimatedWidget slideOutOfRight(Widget widget, Animation animation) {
  const begin = Offset.zero;
  const end = Offset(1.0, 0.0);
  const curve = Curves.ease;
  final tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
  return SlideTransition(
    position: animation.drive(tween),
    child: widget,
  );
}

AnimatedWidget zoomIn(Widget widget, Animation<double> animation) {
  return ScaleTransition(
    scale: Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: animation,
        curve: Curves.fastOutSlowIn,
      ),
    ),
    child: widget,
  );
}

AnimatedWidget zoomOut(Widget widget, Animation<double> animation) {
  return ScaleTransition(
    scale: Tween(
      begin: 1.0,
      end: 0.0,
    ).animate(
      CurvedAnimation(
        parent: animation,
        curve: Curves.fastOutSlowIn,
      ),
    ),
    child: widget,
  );
}
