class HttpException implements Exception {
  final int statusCode;
  final String? message;

  HttpException(this.statusCode, [this.message]);
}

class BadRequestException extends HttpException {
  final String? errorBody;

  BadRequestException(this.errorBody, [message]) : super(400, message);
}

class UnAuthorizedException extends HttpException {
  UnAuthorizedException([message]) : super(401, message);
}
