import 'package:shared_preferences/shared_preferences.dart';

const prefsKeyTheme = "themeMode";

Future<void> saveThemeMode(bool isDark) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setBool(prefsKeyTheme, isDark);
}

Future<bool?> getPreferenceThemeMode() async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.getBool(prefsKeyTheme);
}

Future<bool> isPreferenceDarkTheme() async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.getBool(prefsKeyTheme) == true;
}
