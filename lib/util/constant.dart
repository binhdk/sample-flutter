class Constant {
  static const pageLimit = 50;
  static const minPasswordLength = 8;
}