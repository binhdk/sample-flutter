import 'package:flutter/material.dart';

isPlatformDarkMode(BuildContext context) =>
    MediaQuery.of(context).platformBrightness == Brightness.dark;

isDarkTheme(BuildContext context) =>
    Theme.of(context).brightness == Brightness.dark;

isEmail(String email) => RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$',
    ).hasMatch(email);

hideKeyboard() {
  FocusManager.instance.primaryFocus?.unfocus();
}

unfocus(BuildContext context) {
  FocusScope.of(context).unfocus();
}

isValidPhoneFormat(String phone) =>
    RegExp(r'(^(?:[+0][1-9])?[0-9]{8,15}$)').hasMatch(phone);

T? castOrNull<T>(dynamic x) => x is T ? x : null;
