import 'package:drink_order/res/dimen.dart';
import 'package:drink_order/res/localization/localization.dart';
import 'package:drink_order/res/style/style.dart';
import 'package:drink_order/res/style/text_style.dart';
import 'package:drink_order/widget/rounded_border_container.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeState();
}

class HomeState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: defaultSystemOverlayStyle(context),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              AppLocalizations.of(context).deliver_to,
              style: AppTextStyle.titleSmall,
            ),
            const Text('Ho Chi Minh', style: AppTextStyle.titleSmall),
          ],
        ),
        actions: [
          IconButton(
              onPressed: () {}, icon: const Icon(Icons.favorite_outline)),
        ],
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(64),
          child: RoundedBorderContainer(
            margin: const EdgeInsets.symmetric(horizontal: Dimen.largeOffset),
            width: double.infinity,
            child: Row(
              children: [
                const Icon(Icons.search),
                Text(AppLocalizations.of(context).search_message)
              ],
            ),
          ),
        ),
      ),
      body: Scrollbar(
        child: Column(
          children: [],
        ),
      ),
    );
  }
}
