import 'package:flutter/material.dart';

class FavoritePage extends StatefulWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => FavoriteState();
}

class FavoriteState extends State<FavoritePage> {
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Favorite'),
    );
  }
}
