import 'package:flutter/material.dart';

class MyOrderPage extends StatefulWidget {
  const MyOrderPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => MyOrderState();
}

class MyOrderState extends State<MyOrderPage> {
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('My Order'),
    );
  }
}
