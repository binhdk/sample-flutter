import 'dart:async';

import 'package:drink_order/main/main_page.dart';
import 'package:drink_order/main/user/login_page.dart';
import 'package:drink_order/state/user_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class Launcher extends ConsumerStatefulWidget {
  const Launcher({Key? key}) : super(key: key);

  static const routeName = '/';

  @override
  ConsumerState<Launcher> createState() => LauncherState();
}

class LauncherState extends ConsumerState<Launcher> {
  @override
  void initState() {
    super.initState();
    loadCurrentSession();
  }

  void loadCurrentSession() async {
    Timer(const Duration(milliseconds: 1000), () async {
      final userModel = ref.read(userProvider);
      await userModel.loadUserSessionIfExist();
      if (await userModel.isSessionExist()) {
        if (await userModel.isSessionExpired()) {
          if (!mounted) return;
          Navigator.pushNamedAndRemoveUntil(
              context, LoginPage.routeName, ((route) => false));
        } else {
          if (!mounted) return;
          Navigator.pushNamedAndRemoveUntil(
              context, MainPage.routeName, ((route) => false));
        }
      } else {
        if (!mounted) return;
        Navigator.pushNamedAndRemoveUntil(
            context, LoginPage.routeName, ((route) => false));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: Theme.of(context).colorScheme.primary,
        statusBarColor: Theme.of(context).colorScheme.primary,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light,
      ),
      child: Scaffold(
        backgroundColor: Theme.of(context).colorScheme.primary,
        body: const Center(
          child: Image(image: AssetImage('assets/images/logo_light.png')),
        ),
      ),
    );
  }
}
