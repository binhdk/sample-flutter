import 'package:drink_order/main/user/login_page.dart';
import 'package:drink_order/res/dimen.dart';
import 'package:drink_order/res/localization/localization.dart';
import 'package:drink_order/res/style/button_style.dart';
import 'package:drink_order/res/style/text_style.dart';
import 'package:drink_order/util/constant.dart';
import 'package:drink_order/util/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RegisterPasswordPage extends ConsumerStatefulWidget {
  const RegisterPasswordPage({Key? key}) : super(key: key);

  static const routeName = '/register-password';

  @override
  ConsumerState<RegisterPasswordPage> createState() => RegisterPasswordState();
}

class RegisterPasswordState extends ConsumerState<RegisterPasswordPage> {
  final _formKey = GlobalKey<FormState>();

  final password = TextEditingController();

  registerAccount() async {
    unfocus(context);
    Navigator.of(context)
        .popUntil((route) => route.settings.name == LoginPage.routeName);
  }

  // Initially password is obscure
  bool _obscurePassword = true;

  // Toggles the password show status
  void togglePasswordVisible() {
    setState(() {
      _obscurePassword = !_obscurePassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.chevron_left, size: 32),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: Dimen.horizontalOffset,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 56, bottom: 8),
                  child: Image(
                    image: AssetImage('assets/images/logo.png'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Dimen.xLargeOffset),
                  child: Text(
                    AppLocalizations.of(context).helper_text_password,
                    style: AppTextStyle.bodyLargeBold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Dimen.largeOffset),
                  child: TextFormField(
                    controller: password,
                    obscureText: _obscurePassword,
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.lock_outline),
                      suffixIcon: GestureDetector(
                        child: Icon(_obscurePassword
                            ? Icons.visibility_outlined
                            : Icons.visibility_off_outlined),
                        onTap: () => togglePasswordVisible(),
                      ),
                      labelText: AppLocalizations.of(context).password,
                      helperText: AppLocalizations.of(context)
                          .password_length_error(Constant.minPasswordLength),
                    ),
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return AppLocalizations.of(context).required_field;
                      } else if (value.length < Constant.minPasswordLength) {
                        return AppLocalizations.of(context)
                            .password_length_error(
                          Constant.minPasswordLength,
                        );
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Dimen.xLargeOffset),
                  child: SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: AppButtonStyle.primaryFilledButtonStyle(context),
                      onPressed: () {
                        // Validate returns true if the form is valid, or false otherwise.
                        if (_formKey.currentState!.validate()) {
                          registerAccount();
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            AppLocalizations.of(context).finish,
                            style: AppTextStyle.bodyMediumBold,
                          ),
                          const Icon(Icons.check),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
