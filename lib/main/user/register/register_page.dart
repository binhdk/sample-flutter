import 'package:drink_order/main/user/register/verify_register_phone_page.dart';
import 'package:drink_order/res/dimen.dart';
import 'package:drink_order/res/localization/localization.dart';
import 'package:drink_order/res/style/button_style.dart';
import 'package:drink_order/res/style/text_style.dart';
import 'package:drink_order/util/route/enter_exit_slide_ltr_route.dart';
import 'package:drink_order/util/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RegisterPage extends ConsumerStatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  static const routeName = '/register';

  @override
  ConsumerState<RegisterPage> createState() => RegisterState();
}

class RegisterState extends ConsumerState<RegisterPage> {
  final _formKey = GlobalKey<FormState>();

  final phone = TextEditingController();

  checkPhoneExist(String phone) async {
    unfocus(context);
    Navigator.of(context).push(
      EnterExitSlideLeftToRightRoute(
        routeSettings:
            const RouteSettings(name: VerifyRegisterPhonePage.routeName),
        enterPage: const VerifyRegisterPhonePage(),
        exitPage: widget,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.chevron_left, size: 32),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: Dimen.horizontalOffset,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 56, bottom: 8),
                  child: Image(
                    image: AssetImage('assets/images/logo.png'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Dimen.xLargeOffset),
                  child: Text(
                    AppLocalizations.of(context).title_register_phone,
                    style: AppTextStyle.bodyLargeBold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Dimen.largeOffset),
                  child: TextFormField(
                    controller: phone,
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.phone_android_outlined),
                      labelText: AppLocalizations.of(context).phone,
                    ),
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return AppLocalizations.of(context).required_field;
                      }
                      if (!isValidPhoneFormat(value)) {
                        return AppLocalizations.of(context).invalid_phone;
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Dimen.xLargeOffset),
                  child: SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: AppButtonStyle.primaryFilledButtonStyle(context),
                      onPressed: () {
                        // Validate returns true if the form is valid, or false otherwise.
                        if (_formKey.currentState!.validate()) {
                          checkPhoneExist(phone.text);
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            AppLocalizations.of(context).next,
                            style: AppTextStyle.bodyMediumBold,
                          ),
                          const Icon(Icons.navigate_next_outlined),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
