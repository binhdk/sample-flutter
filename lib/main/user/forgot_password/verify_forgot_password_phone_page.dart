import 'package:drink_order/main/user/forgot_password/set_password_page.dart';
import 'package:drink_order/res/dimen.dart';
import 'package:drink_order/res/localization/localization.dart';
import 'package:drink_order/res/style/button_style.dart';
import 'package:drink_order/res/style/text_style.dart';
import 'package:drink_order/util/route/enter_exit_slide_ltr_route.dart';
import 'package:drink_order/util/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class VerifyForgotPasswordPhonePage extends ConsumerStatefulWidget {
  const VerifyForgotPasswordPhonePage({Key? key}) : super(key: key);

  static const routeName = '/verify-forgot-password-phone';

  @override
  ConsumerState<VerifyForgotPasswordPhonePage> createState() =>
      VerifyForgotPasswordPhoneState();
}

class VerifyForgotPasswordPhoneState
    extends ConsumerState<VerifyForgotPasswordPhonePage> {
  final _formKey = GlobalKey<FormState>();

  final code = TextEditingController();

  verifyCode(String code) async {
    unfocus(context);
    Navigator.of(context).push(
      EnterExitSlideLeftToRightRoute(
        routeSettings: const RouteSettings(name: SetPasswordPage.routeName),
        enterPage: const SetPasswordPage(),
        exitPage: widget,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.chevron_left, size: 32),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: Dimen.horizontalOffset,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 56, bottom: 8),
                  child: Image(
                    image: AssetImage('assets/images/logo.png'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Dimen.xLargeOffset),
                  child: Text(
                    AppLocalizations.of(context).helper_text_otp,
                    style: AppTextStyle.bodyLargeBold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Dimen.largeOffset),
                  child: TextFormField(
                    controller: code,
                    decoration: InputDecoration(
                      labelText: AppLocalizations.of(context).otp,
                    ),
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return AppLocalizations.of(context).required_field;
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Dimen.xLargeOffset),
                  child: SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: AppButtonStyle.primaryFilledButtonStyle(context),
                      onPressed: () {
                        // Validate returns true if the form is valid, or false otherwise.
                        if (_formKey.currentState!.validate()) {
                          verifyCode(code.text);
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            AppLocalizations.of(context).next,
                            style: AppTextStyle.bodyMediumBold,
                          ),
                          const Icon(Icons.navigate_next_outlined),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
