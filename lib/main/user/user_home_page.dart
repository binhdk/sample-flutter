import 'package:drink_order/res/dimen.dart';
import 'package:drink_order/res/localization/localization.dart';
import 'package:drink_order/res/style/button_style.dart';
import 'package:drink_order/res/style/text_style.dart';
import 'package:drink_order/util/util.dart';
import 'package:drink_order/widget/navigate_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info_plus/package_info_plus.dart';

class UserHomePage extends StatefulWidget {
  const UserHomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => UserHomeState();
}

class UserHomeState extends State<UserHomePage> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          systemOverlayStyle: isDarkTheme(context)
              ? SystemUiOverlayStyle(
                  statusBarColor: Theme.of(context).colorScheme.primary,
                  statusBarIconBrightness: Brightness.light,
                  statusBarBrightness: Brightness.light,
                )
              : SystemUiOverlayStyle(
                  statusBarColor: Theme.of(context).colorScheme.primary,
                  statusBarIconBrightness: Brightness.light,
                  statusBarBrightness: Brightness.light,
                ),
          backgroundColor: Theme.of(context).colorScheme.primary,
          pinned: true,
          collapsedHeight: 96,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: false,
            titlePadding: const EdgeInsets.only(left: 16, bottom: 16),
            title: Row(
              children: [
                CircleAvatar(
                  radius: 32,
                  backgroundImage:
                      const AssetImage('assets/images/logo_light.png'),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: Colors.white,
                          width: 1.5,
                        ),
                      ),
                      child: CircleAvatar(
                        backgroundColor: Theme.of(context).colorScheme.primary,
                        radius: 8,
                        child: Icon(
                          Icons.check_sharp,
                          size: 16,
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 4),
                  child: Text(
                    'Nguyen Van Binh',
                    style: Theme.of(context).textTheme.titleMedium!.apply(
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: Column(
            children: [
              NavigateListItem(
                icon: Icon(
                  Icons.sticky_note_2_outlined,
                  color: Colors.blue[600],
                ),
                label: AppLocalizations.of(context).my_vouchers,
                badgeLabel: '12',
                onPress: () {},
              ),
              NavigateListItem(
                icon: Icon(
                  Icons.credit_card_outlined,
                  color: Colors.red[400],
                ),
                label: AppLocalizations.of(context).payment,
                onPress: () {},
              ),
              NavigateListItem(
                icon: Icon(
                  Icons.location_on_outlined,
                  color: Colors.green[400],
                ),
                label: AppLocalizations.of(context).address,
                onPress: () {},
              ),
              Container(
                height: Dimen.mediumOffset,
                color: Theme.of(context).colorScheme.outline,
              ),
              NavigateListItem(
                icon: Icon(
                  Icons.mail_outlined,
                  color: Colors.blueAccent[400],
                ),
                label: AppLocalizations.of(context).invite_friends,
                onPress: () {},
              ),
              NavigateListItem(
                icon: Icon(
                  Icons.help_outline,
                  color: Colors.green[500],
                ),
                label: AppLocalizations.of(context).help,
                onPress: () {},
              ),
              Container(
                height: Dimen.mediumOffset,
                color: Theme.of(context).colorScheme.outline,
              ),
              NavigateListItem(
                icon: Icon(
                  Icons.policy_outlined,
                  color: Colors.blueAccent[400],
                ),
                label: AppLocalizations.of(context).policy,
                onPress: () {},
              ),
              NavigateListItem(
                icon: Icon(
                  Icons.settings_outlined,
                  color: Colors.blueAccent[400],
                ),
                label: AppLocalizations.of(context).settings,
                onPress: () {},
              ),
              NavigateListItem(
                icon: Icon(
                  Icons.info_outline,
                  color: Colors.red[500],
                ),
                label: AppLocalizations.of(context).about_us,
                onPress: () {},
              ),
              Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(
                    horizontal: Dimen.horizontalOffset,
                    vertical: Dimen.verticalOffset),
                child: ElevatedButton(
                  style: AppButtonStyle.primaryFilledButtonStyle(context),
                  onPressed: () {
                    //
                  },
                  child: Text(
                    AppLocalizations.of(context).logout,
                    style: AppTextStyle.bodyMediumBold,
                  ),
                ),
              ),
              FutureBuilder(
                future: PackageInfo.fromPlatform(),
                builder: (context, snapshot) => snapshot.hasData
                    ? Column(
                        children: [
                          Text(
                              '${AppLocalizations.of(context).version} ${(snapshot.data as PackageInfo).version}'),
                          Text((snapshot.data as PackageInfo).appName)
                        ],
                      )
                    : Container(),
              ),
            ],
          ),
        )
      ],
    );
  }
}
