import 'package:drink_order/main/main_page.dart';
import 'package:drink_order/main/user/forgot_password/forgot_password_page.dart';
import 'package:drink_order/main/user/register/register_page.dart';
import 'package:drink_order/res/color.dart';
import 'package:drink_order/res/dimen.dart';
import 'package:drink_order/res/localization/localization.dart';
import 'package:drink_order/res/style/button_style.dart';
import 'package:drink_order/res/style/text_style.dart';
import 'package:drink_order/util/constant.dart';
import 'package:drink_order/util/route/enter_exit_slide_ltr_route.dart';
import 'package:drink_order/util/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class LoginPage extends ConsumerStatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  static const routeName = '/login';

  @override
  ConsumerState<LoginPage> createState() => LoginPageState();
}

class LoginPageState extends ConsumerState<LoginPage> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  final phone = TextEditingController(text: '0902145965');
  final password = TextEditingController(text: '12345678');

  // Initially password is obscure
  bool _obscurePassword = true;

  // Toggles the password show status
  void togglePasswordVisible() {
    setState(() {
      _obscurePassword = !_obscurePassword;
    });
  }

  void loginWithEmailPassword(String email, String password) async {
    //login api
    unfocus(context);
    navigateToHomePage();
  }

  void navigateToRegisterPage() {
    unfocus(context);
    Navigator.of(context).push(
      EnterExitSlideLeftToRightRoute(
        routeSettings: const RouteSettings(name: RegisterPage.routeName),
        enterPage: const RegisterPage(),
        exitPage: widget,
      ),
    );
  }

  void navigateToForgotPasswordPage() {
    unfocus(context);
    Navigator.of(context).push(
      EnterExitSlideLeftToRightRoute(
        routeSettings: const RouteSettings(name: ForgotPasswordPage.routeName),
        enterPage: const ForgotPasswordPage(),
        exitPage: widget,
      ),
    );
  }

  void navigateToHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
      EnterExitSlideLeftToRightRoute(
        routeSettings: const RouteSettings(name: MainPage.routeName),
        enterPage: const MainPage(),
        exitPage: widget,
      ),
      (route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: isDarkTheme(context)
          ? SystemUiOverlayStyle(
              systemNavigationBarColor: AppColor.dark.systemNavigationBarColor,
              statusBarColor: AppColor.dark.statusBarColor,
              statusBarIconBrightness: Brightness.light,
              statusBarBrightness: Brightness.light,
            )
          : SystemUiOverlayStyle(
              systemNavigationBarColor: AppColor.light.systemNavigationBarColor,
              statusBarColor: AppColor.light.statusBarColor,
              statusBarIconBrightness: Brightness.dark,
              statusBarBrightness: Brightness.dark,
            ),
      child: SafeArea(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: Dimen.horizontalOffset,
            ),
            child: Form(
              key: _formKey,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Padding(
                      padding: EdgeInsets.zero,
                      child: Image(
                        image: AssetImage('assets/images/logo.png'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: Dimen.mediumOffset),
                      child: Text(
                        AppLocalizations.of(context).welcome_message,
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                              fontWeight: FontWeight.bold,
                              color: AppColor.of(context).titleColor,
                            ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: Dimen.xLargeOffset),
                      child: TextFormField(
                        controller: phone,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.phone_android_outlined),
                          labelText: AppLocalizations.of(context).phone,
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return AppLocalizations.of(context).required_field;
                          }
                          if (!isValidPhoneFormat(value)) {
                            return AppLocalizations.of(context).invalid_phone;
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: Dimen.largeOffset),
                      child: TextFormField(
                        controller: password,
                        obscureText: _obscurePassword,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.lock_outline),
                          suffixIcon: GestureDetector(
                            child: Icon(_obscurePassword
                                ? Icons.visibility_outlined
                                : Icons.visibility_off_outlined),
                            onTap: () => togglePasswordVisible(),
                          ),
                          labelText: AppLocalizations.of(context).password,
                          helperText: AppLocalizations.of(context)
                              .password_length_error(
                                  Constant.minPasswordLength),
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return AppLocalizations.of(context).required_field;
                          } else if (value.length <
                              Constant.minPasswordLength) {
                            return AppLocalizations.of(context)
                                .password_length_error(
                              Constant.minPasswordLength,
                            );
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: Dimen.largeOffset),
                      child: SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          style:
                              AppButtonStyle.primaryFilledButtonStyle(context),
                          onPressed: () {
                            // Validate returns true if the form is valid, or false otherwise.
                            if (_formKey.currentState!.validate()) {
                              loginWithEmailPassword(phone.text, password.text);
                            }
                          },
                          child: Text(
                            AppLocalizations.of(context).login,
                            style: AppTextStyle.bodyMediumBold,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: Dimen.largeOffset),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          const Divider(thickness: 1),
                          DecoratedBox(
                            decoration: BoxDecoration(
                              color: Theme.of(context).colorScheme.background,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: Dimen.largeOffset,
                              ),
                              child: Text(
                                AppLocalizations.of(context).or.toUpperCase(),
                                style: AppTextStyle.bodyMediumBold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: Dimen.largeOffset),
                      child: SizedBox(
                        width: double.infinity,
                        child: OutlinedButton(
                          style: AppButtonStyle.negativeOutlineButtonStyle(
                            context,
                          ),
                          onPressed: () {
                            //
                          },
                          child: Stack(
                            children: [
                              const Align(
                                alignment: Alignment.centerLeft,
                                child: Image(
                                  image: AssetImage('assets/images/google.png'),
                                ),
                              ),
                              Align(
                                child: Text(
                                  AppLocalizations.of(context)
                                      .login_with_google,
                                  style: AppTextStyle.bodyMediumBold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: Dimen.largeOffset),
                      child: SizedBox(
                        width: double.infinity,
                        child: OutlinedButton(
                          style: AppButtonStyle.negativeOutlineButtonStyle(
                            context,
                          ),
                          onPressed: () {},
                          child: Stack(
                            children: [
                              const Align(
                                alignment: Alignment.centerLeft,
                                child: Image(
                                  image: AssetImage('assets/images/fb.png'),
                                ),
                              ),
                              Align(
                                child: Text(
                                  AppLocalizations.of(context).login_with_fb,
                                  style: AppTextStyle.bodyMediumBold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: Dimen.largeOffset),
                      child: GestureDetector(
                        child: Text(
                          AppLocalizations.of(context).forgot_password,
                          style: AppTextStyle.bodyMediumBold.copyWith(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        onTap: () => navigateToForgotPasswordPage(),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: Dimen.mediumOffset),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(AppLocalizations.of(context).not_have_account),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: Dimen.smallOffset),
                            child: GestureDetector(
                              onTap: () => navigateToRegisterPage(),
                              child: Text(
                                AppLocalizations.of(context).register,
                                style: AppTextStyle.bodyMediumBold.copyWith(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
