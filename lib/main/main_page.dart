import 'package:drink_order/main/cart/cart_page.dart';
import 'package:drink_order/main/home/home_page.dart';
import 'package:drink_order/main/my_order/my_order_page.dart';
import 'package:drink_order/main/notification/notification_page.dart';
import 'package:drink_order/main/user/user_home_page.dart';
import 'package:drink_order/res/localization/localization.dart';
import 'package:drink_order/res/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  static const routeName = '/main';

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentPageIndex = 0;

  final List<Widget> _widgetOptions = [
    const HomePage(),
    const MyOrderPage(),
    const CartPage(),
    const NotificationPage(),
    const UserHomePage()
  ];

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: defaultSystemOverlayStyle(context),
      child: Scaffold(
        body: _widgetOptions.elementAt(currentPageIndex),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(
                color: Theme.of(context).colorScheme.outline,
                width: 1,
              ),
            ),
          ),
          child: BottomNavigationBar(
            selectedFontSize: 12,
            unselectedFontSize: 11.5,
            selectedLabelStyle: const TextStyle(fontWeight: FontWeight.w600),
            backgroundColor: Theme.of(context).colorScheme.background,
            type: BottomNavigationBarType.fixed,
            onTap: (int index) {
              setState(() {
                currentPageIndex = index;
              });
            },
            currentIndex: currentPageIndex,
            items: [
              BottomNavigationBarItem(
                icon: const Icon(Icons.home_outlined),
                label: AppLocalizations.of(context).home,
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.list_alt_outlined),
                label: AppLocalizations.of(context).my_order,
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.shopping_cart_outlined),
                label: AppLocalizations.of(context).cart,
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.notifications_outlined),
                label: AppLocalizations.of(context).notifications,
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.person_outline),
                label: AppLocalizations.of(context).me,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
