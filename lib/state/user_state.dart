import 'dart:convert';

import 'package:drink_order/data/user/token.dart';
import 'package:drink_order/data/user/user.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';

final userModel = UserModel();
final userProvider = ChangeNotifierProvider((ref) => userModel);

class UserModel extends ChangeNotifier {
  static const prefsKeyUser = 'user';
  static const prefsKeyToken = 'token';

  User? _user;
  Token? _token;

  set user(User? user) {
    _user = user;
    notifyListeners();
  }

  User? get user => _user;

  set token(Token? token) {
    _token = token;
    notifyListeners();
  }

  String? get accessToken => _token?.accessToken;

  String accessTokenOrDefault({String defaultValue = ''}) {
    if (_token?.accessToken != null) {
      return _token!.accessToken;
    } else {
      return defaultValue;
    }
  }

  String? get refreshToken => _token?.refreshToken;

  String refreshTokenOrDefault({String defaultValue = ''}) {
    if (_token?.refreshToken != null) {
      return _token!.refreshToken;
    } else {
      return defaultValue;
    }
  }

  Token? get token => _token;

  Future<void> loadUserSessionIfExist() async {
    _user = null;
    _token = null;
    final prefs = await SharedPreferences.getInstance();
    final userString = prefs.getString(prefsKeyUser);
    if (userString != null) {
      _user = User.fromJson(jsonDecode(userString));
    }
    final tokenString = prefs.getString(prefsKeyToken);
    if (tokenString != null) {
      _token = Token.fromJson(jsonDecode(tokenString));
    }
    notifyListeners();
  }

  Future<void> startUserSession(User user, Token token) async {
    _user = user;
    _token = token;
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(prefsKeyUser, jsonEncode(user));
    prefs.setString(prefsKeyToken, jsonEncode(token));
    notifyListeners();
  }

  Future<void> clearSession() async {
    _user = null;
    _token = null;
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove(prefsKeyUser);
    await prefs.remove(prefsKeyToken);
    notifyListeners();
  }

  Future<bool> isSessionExist() async {
    return _user != null && _token != null;
  }

  Future<bool> isSessionExpired() async {
    return _token == null ||
        JwtDecoder.isExpired(_token!.accessToken) ||
        JwtDecoder.isExpired(_token!.refreshToken);
  }
}
