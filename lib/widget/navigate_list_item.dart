import 'package:drink_order/res/color.dart';
import 'package:drink_order/res/dimen.dart';
import 'package:flutter/material.dart';

class NavigateListItem extends StatelessWidget {
  const NavigateListItem({
    Key? key,
    required this.icon,
    required this.label,
    this.badgeLabel = '',
    required this.onPress,
  }) : super(key: key);

  final Icon icon;
  final String label;
  final String badgeLabel;
  final VoidCallback onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(color: Theme.of(context).colorScheme.outline)),
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: Dimen.horizontalOffset,
          vertical: Dimen.largeOffset,
        ),
        child: Row(
          children: [
            icon,
            const SizedBox(width: Dimen.smallOffset),
            Expanded(
              child: Text(
                label,
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .apply(color: AppColor.of(context).titleColor),
              ),
            ),
            const SizedBox(width: Dimen.smallOffset),
            Text(
              badgeLabel,
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .apply(color: AppColor.of(context).titleColor),
            ),
            const SizedBox(width: Dimen.smallOffset),
            Icon(
              Icons.chevron_right_outlined,
              color: AppColor.of(context).titleColor,
            ),
          ],
        ),
      ),
    );
  }
}
