import 'package:drink_order/res/style/style.dart';
import 'package:flutter/material.dart';

class RoundedBorderContainer extends StatelessWidget {
  const RoundedBorderContainer({
    Key? key,
    this.child,
    this.width,
    this.height,
    this.margin,
    this.padding,
    this.alignment,
  }) : super(key: key);

  final Widget? child;
  final double? width;
  final double? height;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final AlignmentGeometry? alignment;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      margin: margin,
      width: width,
      height: height,
      padding: padding ?? const EdgeInsets.all(16),
      decoration: roundBoxDecoration(Theme.of(context).colorScheme.outline),
      child: child,
    );
  }
}
