import 'package:drink_order/data/user/user_api.dart';
import 'package:drink_order/data/user/user_repository.dart';
import 'package:drink_order/state/user_state.dart';

//apis dependencies
final userApi = UserApi();

//repository dependencies
final userRepository = UserRepository(userApi, userModel);
