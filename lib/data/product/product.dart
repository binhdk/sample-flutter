import 'package:json_annotation/json_annotation.dart';

part 'product.g.dart';

@JsonSerializable()
class Product {
  int id;
  @JsonKey(name: 'category_id')
  int categoryId;
  String name;
  int price;
  String description;

  Product(this.id, this.categoryId, this.name, this.price, this.description);

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
