import 'package:drink_order/data/base_response.dart';
import 'package:drink_order/data/user/login_request.dart';
import 'package:drink_order/data/user/login_response.dart';
import 'package:drink_order/data/user/user.dart';
import 'package:drink_order/util/api.dart';
import 'package:http/http.dart' as http;

class UserApi {
  Future<User> getUserProfile(String token) async {
    final response = await http.get(Uri.parse(profileUrl),
        headers: {'Authorization': 'Bearer $token'});

    return BaseResponse.fromHttpResponse(
        response, (json) => User.fromJson(json)).data;
  }

  Future<LoginResponse> login(LoginRequest loginRequest) async {
    final response = await http.post(Uri.parse(loginUrl), body: loginRequest);
    return BaseResponse.fromHttpResponse(
        response, (json) => LoginResponse.fromJson(json)).data;
  }
}
