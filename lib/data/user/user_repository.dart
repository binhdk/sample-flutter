import 'package:drink_order/data/user/login_request.dart';
import 'package:drink_order/data/user/login_response.dart';
import 'package:drink_order/data/user/user.dart';
import 'package:drink_order/data/user/user_api.dart';
import 'package:drink_order/state/user_state.dart';

class UserRepository {
  UserRepository(this.userApi, this.userModel);

  final UserApi userApi;
  final UserModel userModel;

  Future<User> getUserProfile() async {
    return userApi.getUserProfile(userModel.accessTokenOrDefault());
  }

  Future<LoginResponse> login(String email, String password) async {
    return userApi.login(LoginRequest(email, password));
  }
}
