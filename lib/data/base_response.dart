import 'dart:convert';

import 'package:drink_order/util/exceptions.dart';
import 'package:http/http.dart' as http;

class BaseResponse<T> {
  T data;

  BaseResponse(this.data);

  factory BaseResponse.fromJson(Map<String, dynamic> json,
          T Function(Map<String, dynamic> json) fromJsonT) =>
      BaseResponse<T>(
        fromJsonT(json['data']),
      );

  Map<String, dynamic> toJson(Map<String, dynamic> Function(T value) toJsonT) =>
      {
        'data': toJsonT(data),
      };

  factory BaseResponse.fromHttpResponse(
      http.Response response, T Function(Map<String, dynamic> json) fromJsonT) {
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return BaseResponse.fromJson(
        jsonDecode(response.body),
        (json) => fromJsonT(json),
      );
    } else if (response.statusCode == 400) {
      throw BadRequestException(response.body);
    } else if (response.statusCode == 401) {
      throw UnAuthorizedException();
    } else {
      throw HttpException(response.statusCode);
    }
  }
}
