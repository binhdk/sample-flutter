import 'package:drink_order/res/color.dart';
import 'package:flutter/material.dart';

//color state
Color stateColor({bool isDark = false}) =>
    MaterialStateColor.resolveWith((states) => isDark
        ? states.contains(MaterialState.disabled)
            ? AppColor.dark.disabled
            : states.contains(MaterialState.error)
                ? AppColor.dark.error
                : states.contains(MaterialState.focused)
                    ? AppColor.dark.primary
                    : AppColor.dark.textColor
        : states.contains(MaterialState.disabled)
            ? AppColor.light.disabled
            : states.contains(MaterialState.error)
                ? AppColor.light.error
                : states.contains(MaterialState.focused)
                    ? AppColor.light.primary
                    : AppColor.light.textColor);

//color state
Color stateTitleColor({bool isDark = false}) =>
    MaterialStateColor.resolveWith((states) => isDark
        ? states.contains(MaterialState.disabled)
            ? AppColor.dark.disabled
            : states.contains(MaterialState.error)
                ? AppColor.dark.error
                : states.contains(MaterialState.focused)
                    ? AppColor.dark.primary
                    : AppColor.dark.titleColor
        : states.contains(MaterialState.disabled)
            ? AppColor.light.disabled
            : states.contains(MaterialState.error)
                ? AppColor.light.error
                : states.contains(MaterialState.focused)
                    ? AppColor.light.primary
                    : AppColor.light.titleColor);

//style state
TextStyle floatingStateStyle({bool isDark = false}) =>
    MaterialStateTextStyle.resolveWith((states) => isDark
        ? TextStyle(
            color: states.contains(MaterialState.error)
                ? AppColor.dark.error
                : states.contains(MaterialState.focused)
                    ? AppColor.dark.primary
                    : AppColor.dark.textColor,
          )
        : TextStyle(
            color: states.contains(MaterialState.error)
                ? AppColor.light.error
                : states.contains(MaterialState.focused)
                    ? AppColor.light.primary
                    : AppColor.light.textColor,
          ));

//style state
TextStyle stateStyle({bool isDark = false}) => MaterialStateTextStyle.resolveWith(
      (states) => isDark
          ? TextStyle(
              color: states.contains(MaterialState.disabled)
                  ? AppColor.dark.disabled
                  : states.contains(MaterialState.error)
                      ? AppColor.dark.error
                      : states.contains(MaterialState.focused)
                          ? AppColor.dark.primary
                          : AppColor.dark.textColor,
            )
          : TextStyle(
              color: states.contains(MaterialState.disabled)
                  ? AppColor.light.disabled
                  : states.contains(MaterialState.error)
                      ? AppColor.light.error
                      : states.contains(MaterialState.focused)
                          ? AppColor.light.primary
                          : AppColor.light.textColor,
            ),
    );
