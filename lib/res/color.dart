import 'package:drink_order/util/util.dart';
import 'package:flutter/material.dart';

class AppColor {
  ///theme color
  //primary
  final Color primary;
  final Color onPrimary;
  final Color primaryContainer;
  final Color onPrimaryContainer;

  //secondary
  final Color secondary;
  final Color onSecondary;
  final Color secondaryContainer;
  final Color onSecondaryContainer;

  //Tertiary
  final Color tertiary;
  final Color onTertiary;
  final Color tertiaryContainer;
  final Color onTertiaryContainer;

  //surface
  final Color surface;
  final Color onSurface;
  final Color surfaceVariant;
  final Color onSurfaceVariant;

  //outline
  final Color outline;
  final Color outlineDisabled;

  //disabled
  final Color disabled;

  //background
  final Color background;
  final Color onBackground;

  //error
  final Color error;
  final Color onError;
  final Color errorContainer;
  final Color onErrorContainer;

  //inverse
  final Color inverseSurface;
  final Color onInverseSurface;
  final Color inversePrimary;

  //common color
  final Color statusBarColor;
  final Color systemNavigationBarColor;
  final Color textColor;

  final Color titleColor;

  static const Color neutralGreyColor = Color(0xff9098B1);
  static const Color neutralDarkColor = Color(0xff223263);
  static const Color neutralLightColor = Color(0xffEBF0FF);
  static const Color primaryRedColor = Color(0xffFB7181);
  static const Color primaryYellowColor = Color(0xffFFC833);

  const AppColor._({
    //primary
    required this.primary,
    required this.onPrimary,
    required this.primaryContainer,
    required this.onPrimaryContainer,

    //secondary
    required this.secondary,
    required this.onSecondary,
    required this.secondaryContainer,
    required this.onSecondaryContainer,

    //Tertiary
    required this.tertiary,
    required this.onTertiary,
    required this.tertiaryContainer,
    required this.onTertiaryContainer,

    //surface
    required this.surface,
    required this.onSurface,
    required this.surfaceVariant,
    required this.onSurfaceVariant,

    //outline
    required this.outline,
    required this.outlineDisabled,

    //disabled
    required this.disabled,

    //background
    required this.background,
    required this.onBackground,

    //error
    required this.error,
    required this.onError,
    required this.errorContainer,
    required this.onErrorContainer,

    //inverse
    required this.inverseSurface,
    required this.onInverseSurface,
    required this.inversePrimary,

    //common color
    required this.statusBarColor,
    required this.systemNavigationBarColor,
    required this.textColor,
    required this.titleColor,
  });

  factory AppColor.of(BuildContext context) =>
      isDarkTheme(context) ? dark : light;

  factory AppColor.brightness({bool isDark = false}) => isDark ? dark : light;

  static final light = AppColor._(
    //primary
    primary: const Color(0xff40BFFF),
    onPrimary: const Color(0xffffffff),
    primaryContainer: const Color(0xff82c6ee),
    onPrimaryContainer: const Color(0xff081d28),

    //secondary
    secondary: const Color(0xff5d6a73),
    onSecondary: const Color(0xffffffff),
    secondaryContainer: const Color(0xff9fd4f5),
    onSecondaryContainer: const Color(0xff223263),

    //Tertiary
    tertiary: const Color(0xff195577),
    onTertiary: const Color(0xffffffff),
    tertiaryContainer: const Color(0xff95d0f5),
    onTertiaryContainer: const Color(0xff132733),

    //surface
    surface: const Color(0xffffffff),
    onSurface: const Color(0xff9098B1),
    surfaceVariant: const Color(0xffffffff),
    onSurfaceVariant: const Color(0xff9098B1),

    //outline
    outline: const Color(0xffEBF0FF),
    outlineDisabled: const Color(0xff9098B1).withOpacity(0.12),

    //disabled
    disabled: const Color(0xff9098B1).withOpacity(0.38),

    //background
    background: const Color(0xffffffff),
    onBackground: const Color(0xff9098B1),

    //error
    error: const Color(0xFFD32F2F),
    onError: const Color(0xffffffff),
    errorContainer: const Color(0xffF9DEDC),
    onErrorContainer: const Color(0xff1C1B1F),

    //inverse
    inverseSurface: const Color(0xff40BFFF),
    onInverseSurface: const Color(0xffffffff),
    inversePrimary: const Color(0xffffffff),

    //common color
    statusBarColor: const Color(0xffffffff),
    systemNavigationBarColor: const Color(0xff000000),
    textColor: const Color(0xff9098B1),
    titleColor: const Color(0xff223263),
  );

  static final dark = AppColor._(
    //primary
    primary: const Color(0xff40BFFF),
    onPrimary: const Color(0xffffffff),
    primaryContainer: const Color(0xff82c6ee),
    onPrimaryContainer: const Color(0xff081d28),

    //secondary
    secondary: const Color(0xff5d6a73),
    onSecondary: const Color(0xffffffff),
    secondaryContainer: const Color(0xff9fd4f5),
    onSecondaryContainer: const Color(0xff223263),

    //Tertiary
    tertiary: const Color(0xff195577),
    onTertiary: const Color(0xffffffff),
    tertiaryContainer: const Color(0xff95d0f5),
    onTertiaryContainer: const Color(0xff132733),

    //surface
    surface: const Color(0xffffffff),
    onSurface: const Color(0xff9098B1),
    surfaceVariant: const Color(0xffffffff),
    onSurfaceVariant: const Color(0xff9098B1),

    //outline
    outline: const Color(0xffEBF0FF),
    outlineDisabled: const Color(0xff9098B1).withOpacity(0.12),

    //disabled
    disabled: const Color(0xff9098B1).withOpacity(0.38),

    //background
    background: const Color(0xffffffff),
    onBackground: const Color(0xff9098B1),

    //error
    error: const Color(0xFFD32F2F),
    onError: const Color(0xffffffff),
    errorContainer: const Color(0xffF9DEDC),
    onErrorContainer: const Color(0xff1C1B1F),

    //inverse
    inverseSurface: const Color(0xff40BFFF),
    onInverseSurface: const Color(0xffffffff),
    inversePrimary: const Color(0xffffffff),

    //common color
    statusBarColor: const Color(0xffffffff),
    systemNavigationBarColor: const Color(0xff000000),
    textColor: const Color(0xff9098B1),
    titleColor: const Color(0xff223263),
  );
}
