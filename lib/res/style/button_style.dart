import 'package:drink_order/res/color.dart';
import 'package:drink_order/util/util.dart';
import 'package:flutter/material.dart';

class AppButtonStyle {
  static final ButtonStyle elevatedButtonStyle = ElevatedButton.styleFrom(
    padding: const EdgeInsets.symmetric(horizontal: 16),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(5)),
    ),
  );

  static final ButtonStyle outlineButtonStyle = OutlinedButton.styleFrom(
    padding: const EdgeInsets.symmetric(horizontal: 16),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(5)),
    ),
  );
  static final ButtonStyle textButtonButtonStyle = TextButton.styleFrom(
    padding: const EdgeInsets.symmetric(horizontal: 16),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(5)),
    ),
  );

  static ButtonStyle primaryFilledButtonStyle(BuildContext context,
          [Size minimumSize = const Size(64, 56)]) =>
      ElevatedButton.styleFrom(
        // Foreground color
        onPrimary: Theme.of(context).colorScheme.onPrimary,
        // Background color
        primary: Theme.of(context).colorScheme.primary,
        minimumSize: minimumSize,
      );

  static ButtonStyle negativeOutlineButtonStyle(BuildContext context,
          [Size minimumSize = const Size(64, 56)]) =>
      OutlinedButton.styleFrom(
        //for later handle change theme
        primary: isDarkTheme(context)
            ? AppColor.dark.textColor
            : AppColor.light.textColor,
        minimumSize: minimumSize,
      );
}
