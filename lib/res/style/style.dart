import 'package:drink_order/res/color.dart';
import 'package:drink_order/util/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

BoxDecoration roundBoxDecoration(Color color, {double radius = 5}) =>
    BoxDecoration(
      border: Border.all(color: color),
      borderRadius: BorderRadius.all(Radius.circular(radius)),
    );

SystemUiOverlayStyle defaultSystemOverlayStyle(BuildContext context) =>
    isDarkTheme(context)
        ? SystemUiOverlayStyle(
            systemNavigationBarColor: AppColor.dark.systemNavigationBarColor,
            statusBarColor: AppColor.dark.statusBarColor,
            statusBarIconBrightness: Brightness.light,
            statusBarBrightness: Brightness.light,
          )
        : SystemUiOverlayStyle(
            systemNavigationBarColor: AppColor.light.systemNavigationBarColor,
            statusBarColor: AppColor.light.statusBarColor,
            statusBarIconBrightness: Brightness.dark,
            statusBarBrightness: Brightness.dark,
          );
