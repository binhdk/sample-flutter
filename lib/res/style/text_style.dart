import 'package:flutter/material.dart';

class AppTextStyle {
  /* start material style*/
  static const defaultFontFamily = 'OpenSans';

  //display
  static const TextStyle displayLarge = TextStyle(
      fontSize: 57.0,
      fontWeight: FontWeight.w400,
      letterSpacing: -0.25,
      fontFamily: defaultFontFamily);

  static const TextStyle displayMedium = TextStyle(
      fontSize: 45.0,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.0,
      fontFamily: defaultFontFamily);

  static const TextStyle displaySmall = TextStyle(
      fontSize: 36.0,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.0,
      fontFamily: defaultFontFamily);

  //headline
  static const TextStyle headlineLarge = TextStyle(
    fontSize: 32.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.0,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle headlineMedium = TextStyle(
    fontSize: 28.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.0,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle headlineSmall = TextStyle(
    fontSize: 24.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.0,
    fontFamily: defaultFontFamily,
  );

  //title
  static const TextStyle titleLarge = TextStyle(
    fontSize: 22.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.0,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle titleMedium = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.15,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle titleSmall = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.1,
    fontFamily: defaultFontFamily,
  );

  //label
  static const TextStyle labelLarge = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.1,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle labelMedium = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.5,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle labelSmall = TextStyle(
    fontSize: 11.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.5,
    fontFamily: defaultFontFamily,
  );

  //body
  static const TextStyle bodyLarge = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.5,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle bodyMedium = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.25,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle bodySmall = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.4,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle bodyMediumBold = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.25,
    fontFamily: defaultFontFamily,
  );

  static const TextStyle bodyLargeBold = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.5,
    fontFamily: defaultFontFamily,
  );

  /*end material style*/

  //custom here
  //toolbar
  static const TextStyle toolbarTitle = TextStyle(
    fontSize: 20.0,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.4,
    fontFamily: defaultFontFamily,
  );
}
