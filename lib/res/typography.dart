import 'dart:io';

import 'package:drink_order/res/state.dart';
import 'package:flutter/material.dart';

final typography = Typography.material2021(
  platform: Platform.isIOS ? TargetPlatform.iOS : TargetPlatform.android,
  //for light theme
  black: Typography().black.apply(
        bodyColor: stateColor(isDark: true),
        displayColor: stateTitleColor(isDark: true),
      ),
  //for dark theme
  white: Typography.material2021().white.apply(
        bodyColor: stateColor(),
        displayColor: stateTitleColor(),
      ),
);
