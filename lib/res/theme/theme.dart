import 'package:drink_order/res/color.dart';
import 'package:drink_order/res/theme/app_bar_theme.dart';
import 'package:drink_order/res/theme/button_theme.dart';
import 'package:drink_order/res/theme/icon_theme.dart';
import 'package:drink_order/res/theme/input_decoration_theme.dart';
import 'package:drink_order/res/typography.dart';
import 'package:flutter/material.dart';

class AppTheme {
  static final ThemeData theme = ThemeData(
    typography: typography,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    fontFamily: 'OpenSans',
    splashColor: Colors.transparent,
    appBarTheme: appBarTheme(),
    iconTheme: iconThem(),
    elevatedButtonTheme: AppButtonTheme.elevatedButtonTheme,
    outlinedButtonTheme: AppButtonTheme.outlineButtonTheme,
    textButtonTheme: AppButtonTheme.textButtonTheme,
    scaffoldBackgroundColor: AppColor.light.background,
    inputDecorationTheme: AppInputDecorationTheme.inputDecorationTheme(),
    colorScheme: colorScheme,
    useMaterial3: true,
  );
}

final colorScheme = ColorScheme(
  brightness: Brightness.light,
  primary: AppColor.light.primary,
  onPrimary: AppColor.light.onPrimary,
  primaryContainer: AppColor.light.primaryContainer,
  onPrimaryContainer: AppColor.light.onPrimaryContainer,
  secondary: AppColor.light.secondary,
  onSecondary: AppColor.light.onSecondary,
  secondaryContainer: AppColor.light.secondaryContainer,
  onSecondaryContainer: AppColor.light.onSecondaryContainer,
  tertiary: AppColor.light.tertiary,
  onTertiary: AppColor.light.onTertiary,
  tertiaryContainer: AppColor.light.tertiaryContainer,
  onTertiaryContainer: AppColor.light.onTertiaryContainer,
  surface: AppColor.light.surface,
  onSurface: AppColor.light.onSurface,
  surfaceVariant: AppColor.light.surfaceVariant,
  onSurfaceVariant: AppColor.light.onSurfaceVariant,
  background: AppColor.light.background,
  onBackground: AppColor.light.onBackground,
  outline: AppColor.light.outline,
  error: AppColor.light.error,
  onError: AppColor.light.onError,
  errorContainer: AppColor.light.errorContainer,
  onErrorContainer: AppColor.light.onErrorContainer,
  inverseSurface: AppColor.light.inverseSurface,
  onInverseSurface: AppColor.light.onInverseSurface,
  inversePrimary: AppColor.light.inversePrimary,
);
