import 'package:drink_order/res/color.dart';
import 'package:drink_order/res/style/text_style.dart';
import 'package:flutter/material.dart';

AppBarTheme appBarTheme({bool isDark = false}) => isDark
    ? AppBarTheme(
        titleTextStyle: AppTextStyle.titleLarge.copyWith(
          color: AppColor.dark.titleColor,
          fontWeight: FontWeight.w500,
        ),
        toolbarTextStyle: AppTextStyle.titleLarge.copyWith(
          color: AppColor.dark.titleColor,
          fontWeight: FontWeight.w500,
        ),
      )
    : AppBarTheme(
        titleTextStyle: AppTextStyle.titleLarge.copyWith(
          color: AppColor.light.titleColor,
          fontWeight: FontWeight.w500,
        ),
        toolbarTextStyle: AppTextStyle.titleLarge.copyWith(
          color: AppColor.light.titleColor,
          fontWeight: FontWeight.w500,
        ),
      );
