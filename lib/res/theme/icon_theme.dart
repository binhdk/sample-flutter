import 'package:drink_order/res/state.dart';
import 'package:flutter/material.dart';

iconThem({bool isDark = false}) =>
    IconThemeData(color: stateColor(isDark: isDark));
