import 'package:drink_order/res/style/button_style.dart';
import 'package:flutter/material.dart';

class AppButtonTheme {
  static final elevatedButtonTheme = ElevatedButtonThemeData(
    style: AppButtonStyle.elevatedButtonStyle,
  );

  static final outlineButtonTheme = OutlinedButtonThemeData(
    style: AppButtonStyle.outlineButtonStyle,
  );

  static final textButtonTheme = TextButtonThemeData(
    style: AppButtonStyle.textButtonButtonStyle,
  );
}
