import 'package:drink_order/res/color.dart';
import 'package:drink_order/res/state.dart';
import 'package:flutter/material.dart';

class AppInputDecorationTheme {
  static borderSide({bool isDark = false}) => BorderSide(
      color: isDark ? AppColor.dark.outline : AppColor.light.outline);

  static outlineInputBorder({bool isDark = false}) =>
      OutlineInputBorder(borderSide: borderSide(isDark: isDark));

  static inputDecorationTheme({bool isDark = false}) => InputDecorationTheme(
        border: outlineInputBorder(isDark: isDark),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: isDark
                ? AppColor.dark.outlineDisabled
                : AppColor.light.outlineDisabled,
          ),
        ),
        enabledBorder: outlineInputBorder(isDark: isDark),
        labelStyle: stateStyle(isDark: isDark),
        helperStyle: stateStyle(isDark: isDark),
        floatingLabelStyle: floatingStateStyle(isDark: isDark),
        iconColor: stateColor(isDark: isDark),
      );
}
