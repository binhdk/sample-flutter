class Dimen {
  static const verticalOffset = 16.0;
  static const horizontalOffset = 16.0;

  static const xLargeOffset = 24.0;
  static const largeOffset = 16.0;
  static const mediumOffset = 8.0;
  static const smallOffset = 4.0;
}
